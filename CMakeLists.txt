cmake_minimum_required(VERSION 3.9)
project(helios)

set(CMAKE_CXX_STANDARD 17)

find_package(Boost 1.54 COMPONENTS log log_setup thread system filesystem date_time REQUIRED)

include_directories(
        include
        ${Boost_INCLUDE_DIRS}
)

link_directories(include ${Boost_LIBRARY_DIRS})


add_library(networkwatcher
        src/NetworkWatcher/TCPServer.cpp)
target_link_libraries(networkwatcher
        ${Boost_LIBRARIES}
        pthread)


#add_library(queue
#        src/utils/ConcurrentQueue/ConcurrentQueue.cpp)

#add_library(trigger
#        src/utils/Trigger/Trigger.cpp)

add_library(middleware
        src/utils/ConcurrentQueue/ConcurrentQueue.cpp
        src/Middleware/TriggerStringTranslationTools/TriggerStringTranslationTools.cpp
        src/Middleware/Middleware.cpp
        src/utils/BidirectionalChannel/BidirectionalChannel.cpp
        src/utils/Message/Message.cpp
        src/utils/Persistence/Persistence.cpp
        src/Middleware/Constants.cpp
        )


#add_library(controller)
#target_link_libraries(controller
#        python2.7)

target_link_libraries(middleware
        #        trigger
        ${Boost_LIBRARIES}
        pthread)

add_executable(exp_regex src/experiment/regex.cpp)
target_link_libraries(exp_regex ${Boost_LIBRARIES})

add_executable(exp_blocking_tcp_echo_client src/experiment/blocking_tcp_echo_client.cpp)
target_link_libraries(exp_blocking_tcp_echo_client ${Boost_LIBRARIES} pthread)

#add_executable(exp_blocking_tcp_echo_server src/experiment/blocking_tcp_echo_server.cpp)
#target_link_libraries(exp_blocking_tcp_echo_server ${Boost_LIBRARIES})
#
#
#
#add_executable(main main.cpp)
#target_link_libraries(main middleware networkwatcher)
#
#
add_executable(ut_tcpserver src/unit_test/NetworkWatcher/TCPServer.cpp)
target_link_libraries(ut_tcpserver networkwatcher)

add_executable(ut_middleware src/unit_test/Middleware/Middleware.cpp)
target_link_libraries(ut_middleware middleware)

add_executable(integrate1 src/unit_test/integration/integrate1.cpp)
target_link_libraries(integrate1 middleware networkwatcher)

add_executable(ut_pythontask src/unit_test/Controller/PythonTask.cpp)
target_link_libraries(ut_pythontask python2.7)

add_executable(ut_worker src/unit_test/Controller/Worker.cpp)
target_link_libraries(ut_worker python2.7 middleware)

add_executable(ut_controller src/unit_test/Controller/Controller.cpp)
target_link_libraries(ut_controller python2.7 middleware)

add_executable(integrate2 src/unit_test/integration/integrate2.cpp)
#Does this really use python2.7? Or is there a python3.5?
target_link_libraries(integrate2 middleware networkwatcher python2.7)

add_executable(exp_helloworld src/experiment/helloworld.cpp)
add_executable(exp_jsonreader src/experiment/JSONReader.cpp)