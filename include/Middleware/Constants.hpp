//
// Created by soorya on 10/12/17.
//

#ifndef HELIOS_CONSTANTS_HPP
#define HELIOS_CONSTANTS_HPP

#include <string>

enum MODES {

};

const std::string ROOT_WORKER_NAME = "root";

class MessageTypes {
public:
	static const std::string TRIGGER;
	static const std::string ACK;
	static const std::string WARNING;
	static const std::string CONFIRM;
};

class MessageFields {
public:
	static const std::string SUPPRESS_ACK;
	static const std::string SUPPRESS_CONFIRM;
};

class PersistenceFields {
public:
	static const std::string IS_PERSISTENT;
	static const std::string RETRY_INTERVAL;
	static const std::string RETRY_COUNT;
	static const std::string TOLERANCE_COUNT;
};

#endif //HELIOS_CONSTANTS_HPP
