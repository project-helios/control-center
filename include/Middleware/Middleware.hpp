//
// Created by soorya on 6/12/17.
//

#ifndef HELIOS_MIDDLEWARE_HPP
#define HELIOS_MIDDLEWARE_HPP

#include "Middleware/Constants.hpp"
#include "utils/BidirectionalChannel/BidirectionalChannel.hpp"
#include "utils/ConcurrentQueue/ConcurrentQueue.hpp"
#include "utils/Message/Message.hpp"
#include "utils/Trigger/Trigger.hpp"
#include "utils/Response/Response.hpp"
#include "Middleware/TriggerStringTranslationTools/TriggerStringTranslationTools.hpp"
#include <boost/lockfree/queue.hpp>
#include <boost/thread.hpp>

class Middleware {
protected:
	unsigned long long currentSequenceNumber;
	boost::thread thr_monitorInterfaceNW;
	boost::thread thr_monitorInterfaceC;

	bool convertTriggerToMessage(Trigger &, Message &msg);

public:
	BidirectionalChannel<Trigger, Response> interfaceNetworkWatcher;
	BidirectionalChannel<Message, Message> interfaceController;


	Middleware(ConcurrentQueue<Trigger> &incomingNW, ConcurrentQueue<Response> &outgoingNW,
			   ConcurrentQueue<Message> &incomingC, ConcurrentQueue<Message> &outgoingC);

	~Middleware();

	void monitorInterfaceNW();

	void monitorInterfaceC();

	void signalTerminate();
};


#endif //HELIOS_MIDDLEWARE_HPP
