//
// Created by soorya on 20/12/17.
//

#ifndef HELIOS_TRIGGERSTRINGTRANSLATIONTOOLS_HPP
#define HELIOS_TRIGGERSTRINGTRANSLATIONTOOLS_HPP

#include <string>
#include <utils/Identity/Identity.hpp>
#include <map>
#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>

class TriggerStringTranslationTools {
public:
	static bool getIdentity(std::string &str, Identity &identity);

	static bool getWorkerName(std::string &str, std::string &workerName);

	static bool getControllerName(std::string &str, std::string controllerName);

	static void getKwargsAndModifiers(std::string &str, std::map<std::string, std::string> kwargs,
									  std::map<std::string, std::string> modifiers);
};


#endif //HELIOS_TRIGGERSTRINGTRANSLATIONTOOLS_HPP
