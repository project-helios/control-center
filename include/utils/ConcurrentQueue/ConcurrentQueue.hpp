//
// Created by soorya on 7/12/17.
//

#ifndef HELIOS_CONCURRENTQUEUE_HPP
#define HELIOS_CONCURRENTQUEUE_HPP

#include <queue>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition_variable.hpp>
#include <boost/atomic.hpp>

template<typename Data>
class ConcurrentQueue {
private:
	std::queue<Data> the_queue;
	mutable boost::mutex the_mutex;
	boost::condition_variable the_condition_variable;
public:
	void push(Data const &data) {
		boost::mutex::scoped_lock lock(the_mutex);
		the_queue.push(data);
		lock.unlock();
		the_condition_variable.notify_one();
	}

	bool empty() const {
		boost::mutex::scoped_lock lock(the_mutex);
		return the_queue.empty();
	}

	bool try_pop(Data &popped_value) {
		boost::mutex::scoped_lock lock(the_mutex);
		if (the_queue.empty()) {
			return false;
		}
		popped_value = the_queue.front();
		the_queue.pop();
		return true;
	}

	void wait_and_pop(Data &popped_value, int timeout = -1) {
		boost::mutex::scoped_lock lock(the_mutex);
		while (the_queue.empty()) {
			if (timeout == -1) {
				the_condition_variable.wait(lock);
			} else {
				the_condition_variable.wait_for(lock, boost::chrono::duration<int>(timeout));
			}
		}
		popped_value = the_queue.front();
		the_queue.pop();
	}

	~ConcurrentQueue() {
		the_condition_variable.notify_all();
	}
};


#endif //HELIOS_CONCURRENTQUEUE_HPP
