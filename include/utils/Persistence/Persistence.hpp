//
// Created by soorya on 6/12/17.
//

#ifndef HELIOS_PERSISTENCE_HPP
#define HELIOS_PERSISTENCE_HPP

class Persistence {
public:
	bool isPersistent;
	long long retryInterval;
//	TODO Make this "successRetryInterval" and "failureRetryInterval"
	long long retryCount;
	long long toleranceCount;
//	TODO Add bool proceedAfterToleranceExhausted;

	Persistence(bool isPersistent = false, long long retryInterval = -1, long long retryCount = -1,
				long long toleranceCount = -1);
};

#endif //HELIOS_PERSISTENCE_HPP
