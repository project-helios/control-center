//
// Created by soorya on 13/12/17.
//

#ifndef HELIOS_SOCKET_HPP
#define HELIOS_SOCKET_HPP

#include <string>
#include <boost/asio.hpp>

#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>

class Socket {
public:
//	boost::asio::ip::basic_endpoint<boost::asio::ip::tcp> localEndpoint;
//	boost::asio::ip::basic_endpoint<boost::asio::ip::tcp> remoteEndpoint;
//	boost::asio::ip::address sourceAddress;
	int sock;
	std::string sourceAddress;
	int sourcePort;
	std::string destinationAddress;
	int destinationPort;

	Socket() {
		sourcePort = 0;
		destinationPort = 0;
	}

//	Socket(boost::shared_ptr<boost::asio::ip::tcp::socket> sock) {
	Socket(int sock) {
		this->sock = sock;
//		socklen_t len;
//		struct sockaddr_storage addr;
//		char ipstr[INET6_ADDRSTRLEN];
//		int port;
//		len = sizeof addr;
//		getpeername(sock, (struct sockaddr*)&addr, &len);
//
//		 deal with both IPv4 and IPv6:
//		if (addr.ss_family == AF_INET) {
//			auto *s = (struct sockaddr_in *)&addr;
//			port = ntohs(s->sin_port);
//			inet_ntop(AF_INET, &s->sin_addr, ipstr, sizeof ipstr);
//		} else { // AF_INET6
//			auto *s = (struct sockaddr_in6 *)&addr;
//			port = ntohs(s->sin6_port);
//			inet_ntop(AF_INET6, &s->sin6_addr, ipstr, sizeof ipstr);
//		}

//		printf("Peer IP address: %s\n", ipstr);
//		destinationAddress = std::string(ipstr);
//		destinationPort = port;

//		sourceAddress = localEndpoint.address();
//		sourcePort = localEndpoint.port();
//		destinationAddress = remoteEndpoint.address();
//		destinationPort = remoteEndpoint.port();
	}

	static bool blindSend(Socket &mySock, std::string message) {
//		struct sockaddr_in address;
//		int sock = 0, valread;
//		struct sockaddr_in serv_addr;
//		char *hello = "Hello from client";
//		char buffer[1024] = {0};
//		if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
//		{
//			printf("\n Socket creation error \n");
//			return false;
//		}
//
//		memset(&serv_addr, '0', sizeof(serv_addr));
//
//		serv_addr.sin_family = AF_INET;
//		serv_addr.sin_port = htons(mySock.destinationPort);
//
//		 Convert IPv4 and IPv6 addresses from text to binary form
//		if(inet_pton(AF_INET, mySock.destinationAddress.to_string().c_str(), &serv_addr.sin_addr)<=0)
//		{
//			printf("\nInvalid address/ Address not supported \n");
//			return false;
//		}

//		if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
//		{
//			printf("\nConnection Failed \n");
//			return false;
//		}
//		send(sock , message.c_str() , message.length() , 0 );
//		return true;
	}
};


#endif //HELIOS_SOCKET_HPP
