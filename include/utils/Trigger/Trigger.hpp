//
// Created by soorya on 11/12/17.
//

#ifndef HELIOS_TRIGGER_HPP
#define HELIOS_TRIGGER_HPP

#include <string>
#include <boost/asio.hpp>
#include "utils/Trigger/Socket/Socket.hpp"

class Trigger {
public:
	std::string str;
//	boost::shared_ptr<boost::asio::ip::tcp::socket> sock;
	Socket sock;

	Trigger(std::string str, Socket sock) {
		this->str = str;
		this->sock = sock;
	}

	Trigger() {
	}
};


#endif //HELIOS_TRIGGER_HPP
