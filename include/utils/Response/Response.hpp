//
// Created by soorya on 14/12/17.
//

#ifndef HELIOS_RESPONSE_HPP
#define HELIOS_RESPONSE_HPP

#include "utils/Message/Message.hpp"

class Response {
public:
	Message message;
	boost::posix_time::ptime messageCreationDateTime;

	Response(Message message) {
		this->message = message;
		this->messageCreationDateTime = boost::posix_time::second_clock::universal_time();
	}
};

#endif //HELIOS_RESPONSE_HPP
