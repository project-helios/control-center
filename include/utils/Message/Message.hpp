//
// Created by soorya on 6/12/17.
//

#ifndef HELIOS_MESSAGE_HPP
#define HELIOS_MESSAGE_HPP

#include <string>
#include <map>
#include "utils/Persistence/Persistence.hpp"
#include "utils/Trigger/Socket/Socket.hpp"
#include "utils/Identity/Identity.hpp"
#include <boost/date_time.hpp>
#include <boost/asio.hpp>

class Message {
//	TODO Make these protected. Use getters and setters.
public:
	bool isValid;
	Identity sender;
	unsigned long long messageSeqNo;
	std::string messageType;
	boost::posix_time::ptime dateTime;
	std::string controllerName;
	std::string workerName;
	std::map<std::string, std::string> kwargs;
	unsigned long timeout;
	Persistence persistence;
	bool suppressAck;
	bool suppressConfirm;
	std::string response;
//	TODO Remove socket usage? Or, use the IP address for logging purposes.
	Socket sock;

	Message(Identity sender,
			unsigned long long messageSeqNo,
			std::string messageType,
			boost::posix_time::ptime dateTime,
			std::string controllerName,
			std::string workerName,
			std::map<std::string, std::string> kwargs,
			unsigned long timeout,
			Persistence persistence,
			bool suppressAck = false,
			bool suppressConfirm = false,
			std::string response = "",
//			TODO Handle copy constructor usage
			Socket sock = Socket()
	);

	Message();

	Message &operator=(const Message &msg) {
		sender = msg.sender;
		isValid = msg.isValid;
		messageSeqNo = msg.messageSeqNo;
		messageType = msg.messageType;
		dateTime = msg.dateTime;
		controllerName = msg.controllerName;
		workerName = msg.workerName;
		kwargs = msg.kwargs;
		timeout = msg.timeout;
		persistence = msg.persistence;
		suppressAck = msg.suppressAck;
		suppressConfirm = msg.suppressConfirm;
		response = msg.response;
		sock = msg.sock;
		return *this;
	}


};


#endif //HELIOS_MESSAGE_HPP
