//
// Created by soorya on 6/12/17.
//

#ifndef HELIOS_BIDIRECTIONALCHANNEL_HPP
#define HELIOS_BIDIRECTIONALCHANNEL_HPP

#include <boost/lockfree/queue.hpp>
#include "utils/ConcurrentQueue/ConcurrentQueue.hpp"
#include <string>

template<typename T1, typename T2>
class BidirectionalChannel {
public:
	ConcurrentQueue<T1> *inbox;
	ConcurrentQueue<T2> *outbox;

	BidirectionalChannel(ConcurrentQueue<T1> &inbox, ConcurrentQueue<T2> &outbox) {
		this->inbox = &inbox;
		this->outbox = &outbox;
	}

	BidirectionalChannel() {
		this->inbox = nullptr;
		this->outbox = nullptr;
	}
};


#endif //HELIOS_BIDIRECTIONALCHANNEL_HPP
