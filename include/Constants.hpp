//
// Created by soorya on 20/12/17.
//

#ifndef HELIOS_CONSTANTS_HPP
#define HELIOS_CONSTANTS_HPP

#include <string>

namespace Constants {
	namespace Controller {
		namespace Modes {
			constexpr std::string START = "start";
			constexpr std::string STOP = "stop";
		}
	}
	namespace Worker {
//		TODO Use this in the middleware's translation logic.
		constexpr std::string ModeKeyword = "mode";
		namespace Modes {
			constexpr std::string START = "start";
			constexpr std::string STOP = "stop";
		}
	}
}

#endif //HELIOS_CONSTANTS_HPP
