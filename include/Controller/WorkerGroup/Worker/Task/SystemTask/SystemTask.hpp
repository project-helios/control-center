//
// Created by soorya on 16/12/17.
//

#ifndef HELIOS_SYSTEMTASK_HPP
#define HELIOS_SYSTEMTASK_HPP

#include <utility>
#include "Controller/WorkerGroup/Worker/Task/Task.hpp"
#include <python2.7/Python.h>
//TODO Should I use #include <python3.5m/Python.h> ?



class SystemTask : public Task {
public:
	std::string executablePath;
	std::string executableFile;

	SystemTask(std::string executablePath, std::string executableFile, const std::vector<std::string> &kwargs) : Task(kwargs) {
		this->executablePath = std::move(executablePath);
		this->executableFile = std::move(executableFile);
	}

	int execute () final {
//		TODO Why does the program crash if something is wrong in the Python script?
		PyObject *pName, *pModule, *pDict, *pFunc;
		PyObject *pArgs, *pValue;
		int i;

		Py_Initialize();

//		PyRun_SimpleString("print('Hello World from Embedded Python!!!')");
		PyRun_SimpleString("import subprocess");

		std::string scriptPath = ".";
		std::string scriptName = "runProcessAndReturnPID";
		std::string functionName = "startProcess";
		PySys_SetPath((char *) scriptPath.c_str());
		pName = PyString_FromString(scriptName.c_str());
		/* Error checking of pName left out */

		pModule = PyImport_Import(pName);
		Py_DECREF(pName);

		std::vector<std::string> argSet;
		argSet.push_back(executablePath);
		argSet.push_back(executableFile);
		for (const auto &kwarg : kwargSet) {
			argSet.push_back(kwarg);
		}

		if (pModule != NULL) {
			pFunc = PyObject_GetAttrString(pModule, functionName.c_str());
			/* pFunc is a new reference */

			if (pFunc && PyCallable_Check(pFunc)) {
				pArgs = PyTuple_New(argSet.size());
				for (i = 0; i < argSet.size(); ++i) {
					pValue = PyString_FromString(argSet[i].c_str());
					if (!pValue) {
						Py_DECREF(pArgs);
						Py_DECREF(pModule);
						fprintf(stderr, "Cannot convert argument\n");
						return 1;
					}
					/* pValue reference stolen here: */
					PyTuple_SetItem(pArgs, i, pValue);
				}
				pValue = PyObject_CallObject(pFunc, pArgs);
				Py_DECREF(pArgs);
				if (pValue != NULL) {
					printf("Result of call: %ld\n", PyInt_AsLong(pValue));

					processID = static_cast<pid_t>(PyInt_AsLong(pValue));

					Py_DECREF(pValue);
				} else {
					Py_DECREF(pFunc);
					Py_DECREF(pModule);
					PyErr_Print();
					fprintf(stderr, "Call failed\n");
					return 1;
				}
			} else {
				if (PyErr_Occurred())
					PyErr_Print();
				fprintf(stderr, "Cannot find function \"%s\"\n", functionName.c_str());
			}
			Py_XDECREF(pFunc);
			Py_DECREF(pModule);
		} else {
			PyErr_Print();
			fprintf(stderr, "Failed to load \"%s\"\n", scriptPath.c_str());
			return 1;
		}
		Py_Finalize();
		return 0;
	}
};


#endif //HELIOS_SYSTEMTASK_HPP
