//
// Created by soorya on 15/12/17.
//

#ifndef HELIOS_TASK_HPP
#define HELIOS_TASK_HPP

#include <string>
#include <vector>

#include <signal.h>

class Task {
protected:
	pid_t processID;
	std::vector<std::string> kwargSet;

	explicit Task(const std::vector<std::string> &myArguments) {
		// These arguments need to be received in the same order as to be passed into the function.
		processID = 0;
		kwargSet = myArguments;
	}

public:
	virtual int execute()=0;

	virtual bool isExecuting() {
		return processID != 0 && kill(processID, 0) == 0;
	}

	virtual bool terminate() {
		if(isExecuting()) {
			kill(processID, SIGTERM);
			return true;
		}
		processID = 0;
		return false;
	}
};


#endif //HELIOS_TASK_HPP
