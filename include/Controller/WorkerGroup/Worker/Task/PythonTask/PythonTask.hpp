//
// Created by soorya on 16/12/17.
//

#ifndef HELIOS_PYTHONTASK_HPP
#define HELIOS_PYTHONTASK_HPP

#include <utility>
#include "Controller/WorkerGroup/Worker/Task/Task.hpp"

#include <python2.7/Python.h>

class PythonTask : public Task {
public:
	std::string scriptPath;
	std::string scriptName;
	std::string functionName;

	PythonTask(std::string scriptPath, std::string scriptName, std::string functionName,
			   const std::vector<std::string> &kwargs) : Task(kwargs) {
		this->scriptPath = std::move(scriptPath);
		this->scriptName = std::move(scriptName);
		this->functionName = std::move(functionName);
	}

	int execute() final {
		PyObject *pName, *pModule, *pDict, *pFunc;
		PyObject *pArgs, *pValue;
		int i;

		Py_Initialize();

//		PyRun_SimpleString("print('Hello World from Embedded Python!!!')");
		PyRun_SimpleString("import subprocess");

		PySys_SetPath((char *) scriptPath.c_str());
		pName = PyString_FromString(scriptName.c_str());
		/* Error checking of pName left out */

		pModule = PyImport_Import(pName);
		Py_DECREF(pName);

		if (pModule != NULL) {
			pFunc = PyObject_GetAttrString(pModule, functionName.c_str());
			/* pFunc is a new reference */

			if (pFunc && PyCallable_Check(pFunc)) {
//				pArgs = PyTuple_New(argc - 3);
				pArgs = PyTuple_New(kwargSet.size());
				for (i = 0; i < kwargSet.size(); ++i) {
					// pValue = PyInt_FromLong(atoi(argv[i + 3]));
					pValue = PyString_FromString(kwargSet[i].c_str());
					if (!pValue) {
						Py_DECREF(pArgs);
						Py_DECREF(pModule);
						fprintf(stderr, "Cannot convert argument\n");
						return 1;
					}
					/* pValue reference stolen here: */
					PyTuple_SetItem(pArgs, i, pValue);
				}
				pValue = PyObject_CallObject(pFunc, pArgs);
				Py_DECREF(pArgs);
				if (pValue != NULL) {
					printf("Result of call: %ld\n", PyInt_AsLong(pValue));
					Py_DECREF(pValue);
				} else {
					Py_DECREF(pFunc);
					Py_DECREF(pModule);
					PyErr_Print();
					fprintf(stderr, "Call failed\n");
					return 1;
				}
			} else {
				if (PyErr_Occurred())
					PyErr_Print();
				fprintf(stderr, "Cannot find function \"%s\"\n", functionName.c_str());
			}
			Py_XDECREF(pFunc);
			Py_DECREF(pModule);
		} else {
			PyErr_Print();
			fprintf(stderr, "Failed to load \"%s\"\n", scriptPath.c_str());
			return 1;
		}
		Py_Finalize();
		return 0;
	}
};


#endif //HELIOS_PYTHONTASK_HPP
