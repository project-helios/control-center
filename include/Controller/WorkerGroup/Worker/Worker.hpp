//
// Created by soorya on 14/12/17.
//

#ifndef HELIOS_WORKER_HPP
#define HELIOS_WORKER_HPP

#include <string>
#include <utility>
#include <utils/Message/Message.hpp>
#include <utils/BidirectionalChannel/BidirectionalChannel.hpp>
#include <boost/thread.hpp>
#include <Constants.hpp>
#include "utils/Persistence/Persistence.hpp"
#include "Controller/WorkerGroup/Worker/Task/Task.hpp"

class Worker {
public:
	std::string name;
	Persistence persistence;
	Task *task;
	BidirectionalChannel<Message, Message> interfaceController;
	boost::thread thr_monitorInterfaceController;
	boost::thread thr_startTaskExecution;
	boost::thread thr_stopTaskExecution;

	Worker(std::string name, Persistence persistence, Task *task) {
		this->name = std::move(name);
		this->persistence = persistence;
		this->task = task;
		interfaceController = BidirectionalChannel<Message, Message>();
	}

//	TODO Delete all other classes' copy constructors like this.
	Worker(const Worker &worker) = delete;

	void threadifyMonitorInterfaceController() {
		thr_monitorInterfaceController = boost::thread(boost::bind(&Worker::monitorInterfaceController, this));
	}

	void setBidirectionalChannel(ConcurrentQueue<Message> &W2C, ConcurrentQueue<Message> &C2W) {
		interfaceController = BidirectionalChannel<Message, Message>(C2W, W2C);
	}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"

	void monitorInterfaceController() {
		try {
			Message message;
			while (true) {
				boost::this_thread::interruption_point();
				this->interfaceController.inbox->wait_and_pop(message);
				boost::this_thread::interruption_point();
				std::cout << "Worker " << this->name << " popped a message from " << message.sender.username << "."
						  << std::endl;

//				if(message.kwargs.find(Constants::Worker::ModeKeyword) != message.kwargs.end()) {
//					if(message.kwargs[Constants::Worker::ModeKeyword] == Constants::Worker::Modes::START) {
//
//					}
//				}


				thr_startTaskExecution = boost::thread(boost::bind(&Worker::performTask, this));
//				TODO What's the best way to ensure task start? Waiting for that thread to join is blocking.
				thr_startTaskExecution.join();
				message.response = "Worker " + this->name + " responded.";
				boost::this_thread::interruption_point();
				this->interfaceController.outbox->push(message);

				sleep(3);
//				TODO What's the best way to ensure task stop? Waiting for that thread to join is blocking.
				thr_stopTaskExecution = boost::thread(boost::bind(&Worker::abortTask, this));
				thr_stopTaskExecution.join();
				std::cout << "Stopped the task by the worker!" << std::endl;
			}
		} catch (boost::thread_interrupted) {

		}
	}

	void shutdown() {
		thr_monitorInterfaceController.interrupt();
		thr_monitorInterfaceController.join();
	}

	int performTask() {
		if(this->task != nullptr) {
			return this->task->execute();
		}
		return 0;
	}

	bool abortTask() {
		if(this->task != nullptr) {
			return this->task->terminate();
		}
		return true;
	}

	~Worker() {
		std::cout << "Destroying worker with name: " << this->name << std::endl;
		abortTask();
//		if(!abortTask()) {
//			std::cerr << "Worker <" + this->name + "> failed to abort task during destruction." << std::endl;
//		}
	}
#pragma clang diagnostic pop
};


#endif //HELIOS_WORKER_HPP
