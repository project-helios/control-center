//
// Created by soorya on 14/12/17.
//

#ifndef HELIOS_CONTROLLER_HPP
#define HELIOS_CONTROLLER_HPP

#include <utils/BidirectionalChannel/BidirectionalChannel.hpp>
#include <utils/Message/Message.hpp>
#include <vector>
#include <Controller/WorkerGroup/Worker/Worker.hpp>
#include <boost/thread.hpp>

class WorkerAndCommsPointers {
public:
	Worker *worker;
	BidirectionalChannel<Message, Message> *bidirectionalChannel;

	WorkerAndCommsPointers(Worker *worker, BidirectionalChannel<Message, Message> *bidirectionalChannel) {
		this->worker = worker;
		this->bidirectionalChannel = bidirectionalChannel;
	}

	void DeleteBidirectionalChannel() {
		if (bidirectionalChannel != nullptr) {
			delete bidirectionalChannel;
			bidirectionalChannel = nullptr;
		}
	}
};

class Controller {
private:
	std::vector<ConcurrentQueue<Message> > C2Ws;
	std::vector<ConcurrentQueue<Message> > W2Cs;
public:
	BidirectionalChannel<Message, Message> interfaceMiddleware;
	std::vector<WorkerAndCommsPointers> workersAndComms;
	std::vector<boost::thread> workerThreads;
	boost::thread thr_monitorInterfaceMiddleware;

	Controller(ConcurrentQueue<Message> &MW2C, ConcurrentQueue<Message> &C2MW, std::vector<Worker *> &workers) {
		interfaceMiddleware = BidirectionalChannel<Message, Message>(MW2C, C2MW);

		C2Ws = std::vector<ConcurrentQueue<Message> >(workers.size());
		W2Cs = std::vector<ConcurrentQueue<Message> >(workers.size());

		for (unsigned int i = 0; i < workers.size(); i++) {
			auto *interfaceWorker = new BidirectionalChannel<Message, Message>(W2Cs[i], C2Ws[i]);
			workers[i]->setBidirectionalChannel(W2Cs[i], C2Ws[i]);
			workers[i]->threadifyMonitorInterfaceController();
			WorkerAndCommsPointers workerAndCommsPointers = WorkerAndCommsPointers(workers[i], interfaceWorker);
			workersAndComms.push_back(std::move(workerAndCommsPointers));
			workerThreads.push_back(
					std::move(boost::thread(boost::bind(&Controller::monitorInterfaceWorker, this, i))));
		}
		thr_monitorInterfaceMiddleware = boost::thread(boost::bind(&Controller::monitorInterfaceMiddleware, this));
	}

	~Controller() {
		for (unsigned int i = 0; i < workersAndComms.size(); i++) {
			workersAndComms[i].DeleteBidirectionalChannel();
		}
	}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"

	void monitorInterfaceWorker(unsigned int workerIndex) {
		try {
			Message message;
			while (true) {
				this->workersAndComms[workerIndex].bidirectionalChannel->inbox->wait_and_pop(message);
				boost::this_thread::interruption_point();
				this->interfaceMiddleware.outbox->push(message);
				boost::this_thread::interruption_point();
			}
		} catch (boost::thread_interrupted) {

		}
	}

#pragma clang diagnostic pop

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"

	void monitorInterfaceMiddleware() {
		std::cout << "Controller starting to monitor middleware interface!" << std::endl;
		try {
			Message message;
			while (true) {
				boost::this_thread::interruption_point();
				this->interfaceMiddleware.inbox->wait_and_pop(message);
				boost::this_thread::interruption_point();
				std::cout << "Controller received a message. Target worker: " << message.workerName << std::endl;
				boost::this_thread::interruption_point();
				for (unsigned int i = 0; i < workersAndComms.size(); i++) {
					if (workersAndComms[i].worker->name == message.workerName) {
						workersAndComms[i].bidirectionalChannel->outbox->push(message);
					}
				}
			}
		} catch (boost::thread_interrupted) {

		}
	}

#pragma clang diagnostic pop

	void shutdown() {
		thr_monitorInterfaceMiddleware.interrupt();
//		for (unsigned int i = 0; i < workerThreads.size(); i++) {
//			workerAndComms[i].first.shutdown();
//			workerThreads[i].interrupt();
//		}
		for (unsigned int i = 0; i < workersAndComms.size(); i++) {
			workersAndComms[i].worker->shutdown();
			workerThreads[i].interrupt();
		}

		thr_monitorInterfaceMiddleware.join();
		for (unsigned int i = 0; i < workerThreads.size(); i++) {
			workerThreads[i].join();
		}
	}
};


#endif //HELIOS_CONTROLLER_HPP
