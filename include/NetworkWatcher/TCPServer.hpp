//
// Created by soorya on 3/12/17.
//

//
// blocking_tcp_echo_server.cpp
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2014 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include <cstdlib>
#include <iostream>
#include "utils/BidirectionalChannel/BidirectionalChannel.hpp"
#include <boost/bind.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/thread/thread.hpp>
#include <utils/Trigger/Trigger.hpp>
#include "utils/Response/Response.hpp"

using boost::asio::ip::tcp;

const int max_length = 1024;

typedef boost::shared_ptr<tcp::socket> socket_ptr;

class TCPServer {
protected:
	unsigned short port;
	boost::asio::io_service io_service;
	boost::thread thr_server;
	boost::thread thr_response;
public:
	BidirectionalChannel<Response, Trigger> interfaceMiddleware;

	TCPServer(unsigned short newPort, ConcurrentQueue<Response> &incoming, ConcurrentQueue<Trigger> &outgoing);

//	void session(socket_ptr sock);
	void session(int sock);

	void server();

	void initiateServer();

	void shutdown();

	void responseMechanism();
};
