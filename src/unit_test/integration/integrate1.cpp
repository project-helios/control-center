//
// Created by soorya on 10/12/17.
//


#include <iostream>
//#include <utils/ConcurrentQueue/ConcurrentQueue.hpp>
#include "Middleware/Middleware.hpp"
#include "NetworkWatcher/TCPServer.hpp"

int main() {

	ConcurrentQueue<Trigger> NW2MW;
	ConcurrentQueue<Response> MW2NW;
	ConcurrentQueue<Message> C2MW;
	ConcurrentQueue<Message> MW2C;

	TCPServer tcpServer(8081, MW2NW, NW2MW);
	std::cout << "Launching server..." << std::endl;
	tcpServer.initiateServer();
	std::cout << "Launched server!" << std::endl;

	std::cout << "Running integrate1 unit test!" << std::endl;

	Middleware middleware(NW2MW, MW2NW, C2MW, MW2C);
	sleep(1);
	std::string trigger = "[maha]fn_sq@helios:mode=<myMode>,kwarg1=<myKwarg1>,kwarg2=<myKwar@g2> --timeout=<3> --isPersistent=<true>";
	Message popped;

	std::cout << "Start." << std::endl;
//	NW2MW.push(trigger);
//	std::cout << "Pushed: " << trigger << std::endl;
	MW2C.wait_and_pop(popped);
	std::cout << "$$$ Popped message with workerName: " << popped.workerName << std::endl;
//	sleep(2);
	C2MW.push(popped);
	std::cout << "Pushed it back!" << std::endl;
//	sleep(1);
//	Trigger response;
//	MW2NW.wait_and_pop(response);
//	std::cout << "$$$ Received: " << response.str << std::endl;
	sleep(2);
	middleware.signalTerminate();
//		middleware.();
	tcpServer.shutdown();
	std::cout << "Exiting main tester." << std::endl;
	sleep(2);
	return 0;
}