//
// Created by soorya on 19/12/17.
//

// Trying to integrate Controller, middleware, and networkwatcher.


#include <iostream>
#include <Controller/WorkerGroup/Worker/Task/PythonTask/PythonTask.hpp>
#include <Controller/WorkerGroup/Worker/Task/SystemTask/SystemTask.hpp>
//#include <utils/ConcurrentQueue/ConcurrentQueue.hpp>
#include "Middleware/Middleware.hpp"
#include "NetworkWatcher/TCPServer.hpp"
#include "Controller/Controller.hpp"

int main() {

	ConcurrentQueue<Trigger> NW2MW;
	ConcurrentQueue<Response> MW2NW;
	ConcurrentQueue<Message> C2MW;
	ConcurrentQueue<Message> MW2C;

	TCPServer tcpServer(8081, MW2NW, NW2MW);
	std::cout << "Launching server..." << std::endl;
	tcpServer.initiateServer();
	std::cout << "Launched server!" << std::endl;


//	PythonTask pythonTask = PythonTask("/home/soorya/Desktop/", "fitbitSync", "execute", std::vector<std::string>());
//	TODO Get these tasks from a configuration file. Avoid recompilation of this program.
//	PythonTask pythonTask("/home/soorya/Desktop/", "fitbitSync", "dummy", std::vector<std::string>());
//	Persistence defaultPersistence(false, 3, 4, 0);
//	Worker worker("fitbitsync", defaultPersistence, &pythonTask);

	SystemTask systemTask("./", "exp_helloworld", std::vector<std::string>());
	Worker worker("helloworld", Persistence(), &systemTask);

	std::vector<Worker *> workers;
	workers.push_back(&worker);

	Controller controller(MW2C, C2MW, workers);

	std::cout << "Running integrate2 unit test!" << std::endl;

	Middleware middleware(NW2MW, MW2NW, C2MW, MW2C);
//	sleep(1);
	std::string trigger = "[b1u]fitbitsync@helios:mode=<myMode>,kwarg1=<myKwarg1>,kwarg2=<myKwar@g2> --timeout=<3> --isPersistent=<true>";
	std::string trigger2 = "[b1u]helloworld@helios:mode=<myMode>,kwarg1=<myKwarg1>,kwarg2=<myKwar@g2> --timeout=<3> --isPersistent=<true>";
//	Message popped;

	std::cout << "Start." << std::endl;
//	NW2MW.push(trigger);
//	std::cout << "Pushed: " << trigger << std::endl;
//	MW2C.wait_and_pop(popped);
//	std::cout << "$$$ Popped message with workerName: " << popped.workerName << std::endl;
//	sleep(2);
//	C2MW.push(popped);
//	std::cout << "Pushed it back!" << std::endl;
//	sleep(1);
//	Trigger response;
//	MW2NW.wait_and_pop(response);
//	std::cout << "$$$ Received: " << response.str << std::endl;
	sleep(10);
	std::cout << "Shutting down!" << std::endl;
	middleware.signalTerminate();
	tcpServer.shutdown();
	controller.shutdown();
	std::cout << "Exiting main tester." << std::endl;
	sleep(2);
	return 0;
}