//
// Created by soorya on 19/12/17.
//

#include <Controller/Controller.hpp>
#include <Controller/WorkerGroup/Worker/Task/PythonTask/PythonTask.hpp>
#include <string>
#include <vector>

using namespace std;

int main() {
	PythonTask pythonTask = PythonTask("/home/soorya/Desktop/", "fitbitSync", "execute", vector<string>());
	Worker worker = Worker("fitbitsync", Persistence(), &pythonTask);

	vector<Worker> workers;
	workers.push_back(std::move(worker));

	ConcurrentQueue<Message> M2C, C2M;
	Controller controller = Controller(M2C, C2M, workers);

	Message message = Message();
	M2C.push(message);
	cout << "Pushed an empty message!" << endl;
	C2M.wait_and_pop(message);
	cout << "Popped response: " << message.response << endl;
	return 0;
}