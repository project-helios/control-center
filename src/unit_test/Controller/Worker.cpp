//
// Created by soorya on 19/12/17.
//

#include "Controller/WorkerGroup/Worker/Worker.hpp"
#include <iostream>
#include <Controller/WorkerGroup/Worker/Task/PythonTask/PythonTask.hpp>

using namespace std;

int main() {
	PythonTask pythonTask = PythonTask("/home/soorya/Desktop/", "fitbitSync", "execute", vector<string>());
	Worker worker = Worker("fitbitsync", Persistence(), &pythonTask);
	worker.task->execute();
}
