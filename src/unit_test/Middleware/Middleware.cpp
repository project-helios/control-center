//
// Created by soorya on 6/12/17.
//

#include <iostream>
//#include <utils/ConcurrentQueue/ConcurrentQueue.hpp>
#include "Middleware/Middleware.hpp"

int main() {
	std::cout << "Running middleware unit test!" << std::endl;
	ConcurrentQueue<std::string> incomingNW;
	ConcurrentQueue<std::string> outgoingNW;
	ConcurrentQueue<Message> incomingC;
	ConcurrentQueue<Message> outgoingC;
	Middleware middleware(incomingNW, outgoingNW, incomingC, outgoingC);
	sleep(2);
	std::string trigger = "fn_sq@helios:mode=<myMode>,kwarg1=<myKwarg1>,kwarg2=<myKwar@g2> --timeout=<3> --isPersistent=<true>";
	Message popped(
			1,
			"trigger",
			boost::posix_time::second_clock::universal_time(),
			"controller",
			"mode",
			std::map<std::string, std::string>(),
			9,
			Persistence()
	);

	std::cout << "Start." << std::endl;
	incomingNW.push(trigger);
	std::cout << "Pushed: " << trigger << std::endl;
	outgoingC.wait_and_pop(popped);
	std::cout << "$$$ Popped message with workerName: " << popped.workerName << std::endl;
//	sleep(2);
	incomingC.push(popped);
	std::cout << "Pushed it back!" << std::endl;
	sleep(1);
	std::string response;
	outgoingNW.wait_and_pop(response);
	std::cout << "$$$ Received: " << response << std::endl;
	sleep(2);
//	middleware.signalTerminate();
	std::cout << "Exiting main tester." << std::endl;
	return 0;
}