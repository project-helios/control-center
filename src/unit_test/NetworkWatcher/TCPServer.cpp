//
// Created by soorya on 3/12/17.
//

#include "NetworkWatcher/TCPServer.hpp"

int main(int argc, char *argv[]) {
	try {
		ConcurrentQueue<std::string> NW2MW;
		ConcurrentQueue<std::string> MW2NW;

		TCPServer tcpServer(8081, MW2NW, NW2MW);
		std::cout << "Launching server..." << std::endl;
		tcpServer.initiateServer();
		std::cout << "Launched server!" << std::endl;
		sleep(15);
		tcpServer.shutdown();
	}
	catch (std::exception &e) {
		std::cerr << "Exception: " << e.what() << "\n";
	}

	return 0;
}