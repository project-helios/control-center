//
// Created by soorya on 3/12/17.
//

#include "NetworkWatcher/TCPServer.hpp"
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>

TCPServer::TCPServer(unsigned short newPort, ConcurrentQueue<Response> &incoming, ConcurrentQueue<Trigger> &outgoing) {
	port = newPort;
	interfaceMiddleware = BidirectionalChannel<Response, Trigger>(incoming, outgoing);
}

//void TCPServer::session(socket_ptr sock) {
void TCPServer::session(int new_socket) {
	try {
		while (true) {
			char data[max_length];

//			boost::system::error_code error;
//			size_t length = sock->read_some(boost::asio::buffer(data), error);
			ssize_t valread = read(new_socket, data, 1024);
//			if (error == boost::asio::error::eof)
			if (valread <= 0)
				break; // Connection closed cleanly by peer.
//			else if (error)
//				throw boost::system::system_error(error); // Some other error.

//			interfaceMiddleware.outbox->push(std::string(data));



//			interfaceMiddleware.outbox->push(Trigger(std::string(data), sock));
			interfaceMiddleware.outbox->push(Trigger(std::string(data), Socket(new_socket)));
			std::cout << "Pushed data from NW to MW!" << std::endl;
			send(new_socket, "sample response", sizeof("sample response"), 0);
//			boost::asio::write(*sock, boost::asio::buffer(data, length));
//			std::cout << "Experimenting: " << sock->remote_endpoint().address() << "::"
//					  << sock->remote_endpoint().port() << std::endl;
//			std::cout << "Experimenting: " << sock->max_connections << std::endl;
//			std::cout << "Experimenting: " << sock->is_open() << std::endl;


		}
	}
	catch (std::exception &e) {
		std::cerr << "Exception in thread: " << e.what() << "\n";
	}
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"

void TCPServer::server() {

	int server_fd, new_socket, valread;
	struct sockaddr_in address;
	int opt = 1;
	int addrlen = sizeof(address);

	// Creating socket file descriptor
	if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
		perror("socket failed");
//		exit(EXIT_FAILURE);
		return;
	}

	// Forcefully attaching socket to the port 8080
	if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
		perror("setsockopt");
//		exit(EXIT_FAILURE);
		return;
	}
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons(port);

	// Forcefully attaching socket to the port 8080
	if (bind(server_fd, (struct sockaddr *) &address, sizeof(address)) < 0) {
		perror("bind failed");
//		exit(EXIT_FAILURE);
		return;
	}
	if (listen(server_fd, 3) < 0) {
		perror("listen");
		exit(EXIT_FAILURE);
	}
	while (true) {
		boost::this_thread::interruption_point();
		if ((new_socket = accept(server_fd, (struct sockaddr *) &address, (socklen_t *) &addrlen)) < 0) {
			perror("accept");
			exit(EXIT_FAILURE);
		}
		boost::this_thread::interruption_point();
//		TODO Add these threads to a vector of threads.
		boost::thread t(boost::bind(&TCPServer::session, this, new_socket));
		std::cout << "Accepted a connection!" << std::endl;
	}

//	valread = read( new_socket , buffer, 1024);
//	printf("%s\n",buffer );
//	send(new_socket , hello , strlen(hello) , 0 );
//	printf("Hello message sent\n");
//	return 0;


//	tcp::acceptor a(io_service, tcp::endpoint(tcp::v4(), port));
//	while (true) {
//		boost::this_thread::interruption_point();
//		socket_ptr sock(new tcp::socket(io_service));
//		boost::this_thread::interruption_point();
//		a.accept(*sock);
//		boost::this_thread::interruption_point();
//		TODO Add these threads to a vector of threads.
//		boost::thread t(boost::bind(&TCPServer::session, this, sock));
//		std::cout << "Accepted a connection!" << std::endl;
//	}
}

#pragma clang diagnostic pop

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"

void TCPServer::responseMechanism() {
	while (true) {
		boost::this_thread::interruption_point();
		Response response = Response(Message());
//		TODO Test this wait with a timeout.
		interfaceMiddleware.inbox->wait_and_pop(response);
		boost::this_thread::interruption_point();
		std::cout << "NetworkWatcher handled a response! Username: " << response.message.sender.username << std::endl;
		std::cout << "NetworkWatcher handled a response! Response: " << response.message.response << std::endl;
//		std::cout << "Ready to send a response to " << trigger.sock.destinationAddress << ":" << trigger.sock.destinationPort << std::endl;
//		std::cout << "Ready to send a response from " << trigger.sock.sourceAddress << ":" << trigger.sock.sourcePort << std::endl;
//		Socket::blindSend(trigger.sock, trigger.str);
//		ssize_t sentByteCount = send(trigger.sock.sock , trigger.str.c_str() , trigger.str.length() , 0 );

//		boost::asio::write(*(trigger.sock), boost::asio::buffer(trigger.str, trigger.str.length()));
//		std::cout << "Sent a response of " << sentByteCount << " bytes" << std::endl;
//		std::cout << "Checking: " << trigger.sock->is_open() << std::endl;
//		trigger.sock->write_some(boost::asio::buffer("Writing something"));
//		sleep(1);
	}
}

#pragma clang diagnostic pop

void TCPServer::initiateServer() {
	thr_server = boost::thread(boost::bind(&TCPServer::server, this));
	thr_response = boost::thread(boost::bind(&TCPServer::responseMechanism, this));
}

void TCPServer::shutdown() {
	std::cout << "Shutting down server..." << std::endl;
	thr_server.interrupt();
	thr_response.interrupt();
//	TODO Check if I can use join() to shutdown.
//	thr_server.join();
	thr_server.detach();
	std::cout << "Detached the server thread." << std::endl;
	thr_response.join();
	std::cout << "Joined the response thread." << std::endl;
//	thr_response.detach();
	std::cout << "Shut down server!" << std::endl;
}
