//
// Created by soorya on 7/12/17.
//

#include "utils/ConcurrentQueue/ConcurrentQueue.hpp"
#include "utils/Message/Message.hpp"

//template<>
//void ConcurrentQueue<Message>::push(Message const &data) {
//	boost::mutex::scoped_lock lock(the_mutex);
//	the_queue.push(data);
//	lock.unlock();
//	the_condition_variable.notify_one();
//}

//template<>
//bool ConcurrentQueue<Message>::empty() const {
//	boost::mutex::scoped_lock lock(the_mutex);
//	return the_queue.empty();
//}


//template<>
//bool ConcurrentQueue<Message>::try_pop(Message &popped_value) {
//	boost::mutex::scoped_lock lock(the_mutex);
//	if (the_queue.empty()) {
//		return false;
//	}
//	popped_value = the_queue.front();
//	the_queue.pop();
//	return true;
//}


//template<>
//void ConcurrentQueue<Message>::wait_and_pop(Message &popped_value, int timeout) {
//	boost::mutex::scoped_lock lock(the_mutex);
//	while (the_queue.empty()) {
//		if (timeout == -1) {
//			the_condition_variable.wait(lock);
//		} else {
//			the_condition_variable.wait_for(lock, boost::chrono::duration<int>(timeout));
//		}
//	}
//	popped_value = the_queue.front();
//	the_queue.pop();
//}