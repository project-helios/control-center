//
// Created by soorya on 6/12/17.
//

#include "utils/Persistence/Persistence.hpp"

Persistence::Persistence(bool isPersistent, long long retryInterval, long long retryCount, long long toleranceCount) {
	this->isPersistent = isPersistent;
	this->retryInterval = retryInterval;
	this->retryCount = retryCount;
	this->toleranceCount = toleranceCount;
}
