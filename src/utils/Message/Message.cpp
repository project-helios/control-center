//
// Created by soorya on 6/12/17.
//

#include "utils/Message/Message.hpp"

Message::Message(
		Identity sender,
		unsigned long long messageSeqNo, std::string messageType,
		boost::posix_time::ptime dateTime, std::string controllerName,
		std::string workerName,
		std::map<std::string, std::string> kwargs,
		unsigned long timeout, Persistence persistence,
		bool suppressAck, bool suppressConfirm, std::string response,
		Socket sock) {
	this->sender = sender;
	this->isValid = true;
	this->messageSeqNo = messageSeqNo;
	this->messageType = messageType;
	this->dateTime = dateTime;
	this->controllerName = controllerName;
	this->workerName = workerName;
	this->kwargs = kwargs;
	this->timeout = timeout;
	this->persistence = persistence;
	this->suppressAck = suppressAck;
	this->suppressConfirm = suppressConfirm;
	this->response = response;
	this->sock = sock;
}

Message::Message() {
	this->sender.username = "noUsername";
	this->isValid = false;
	this->messageSeqNo = 0;
	this->messageType = "";
	this->dateTime = boost::posix_time::second_clock::universal_time();
	this->controllerName = "noController";
	this->workerName = "noWorker";
	this->kwargs = std::map<std::string, std::string>();
	this->timeout = 0;
	this->persistence = Persistence();
	this->suppressAck = true;
	this->suppressConfirm = true;
	this->response = "";
	this->sock = Socket();
}
