//
// Created by soorya on 6/12/17.
//

#include "Middleware/Middleware.hpp"

Middleware::Middleware(ConcurrentQueue<Trigger> &incomingNW, ConcurrentQueue<Response> &outgoingNW,
					   ConcurrentQueue<Message> &incomingC, ConcurrentQueue<Message> &outgoingC) {
	currentSequenceNumber = 1;
	interfaceController = BidirectionalChannel<Message, Message>(incomingC, outgoingC);
	interfaceNetworkWatcher = BidirectionalChannel<Trigger, Response>(incomingNW, outgoingNW);
	thr_monitorInterfaceNW = boost::thread(boost::bind(&Middleware::monitorInterfaceNW, this));
	thr_monitorInterfaceC = boost::thread(boost::bind(&Middleware::monitorInterfaceC, this));
}


void Middleware::monitorInterfaceNW() {
	try {
		Message message;
		Trigger poppedTrigger;
		while (true) {
			boost::this_thread::interruption_point();
			this->interfaceNetworkWatcher.inbox->wait_and_pop(poppedTrigger, 1);
			convertTriggerToMessage(poppedTrigger, message);
			boost::this_thread::interruption_point();
			this->interfaceController.outbox->push(message);
		}
	}
	catch (boost::thread_interrupted) {
	}
}

void Middleware::monitorInterfaceC() {
	try {
		Message poppedMessage;
		while (true) {
			boost::this_thread::interruption_point();
			this->interfaceController.inbox->wait_and_pop(poppedMessage, 1);
			boost::this_thread::interruption_point();
//			poppedMessage.response = "NEED_TO_CREATE_A_RESPONSE";
			Response response = Response(poppedMessage);
//			std::cout << "monitorInterfaceC about to push a trigger..." << std::endl;
			this->interfaceNetworkWatcher.outbox->push(response);
			std::cout << "monitorInterfaceC pushed a trigger!" << std::endl;
		}
	}
	catch (boost::thread_interrupted) {
	}
}


Middleware::~Middleware() {
	thr_monitorInterfaceNW.interrupt();
	thr_monitorInterfaceC.interrupt();
	thr_monitorInterfaceNW.join();
	thr_monitorInterfaceC.join();
}

void Middleware::signalTerminate() {
	thr_monitorInterfaceNW.interrupt();
	thr_monitorInterfaceC.interrupt();
}

bool Middleware::convertTriggerToMessage(Trigger &trigger, Message &message) {
	message.isValid = false;
	std::string str = trigger.str;
	Message msg;
	msg.isValid = true;
	msg.messageSeqNo = currentSequenceNumber;
	currentSequenceNumber++;
	msg.messageType = MessageTypes::TRIGGER;
	msg.dateTime = boost::posix_time::second_clock::universal_time();

	Identity identity;
	if (TriggerStringTranslationTools::getIdentity(str, identity)) {
		msg.sender = identity;
	} else {
		return false;
	}

	std::string controllerName;
	if (TriggerStringTranslationTools::getControllerName(str, controllerName)) {
		msg.controllerName = controllerName;
	} else {
		return false;
	}
	std::string workerName;
	if (TriggerStringTranslationTools::getWorkerName(str, workerName)) {
		msg.workerName = workerName;
	} else {
		return false;
	}
	std::map<std::string, std::string> kwargs, modifiers;
	TriggerStringTranslationTools::getKwargsAndModifiers(str, kwargs, modifiers);
	msg.kwargs = kwargs;

	Persistence persistence;
	if (modifiers.find(PersistenceFields::IS_PERSISTENT) != modifiers.end()) {
		boost::to_lower(modifiers[PersistenceFields::IS_PERSISTENT]);
		persistence.isPersistent = modifiers[PersistenceFields::IS_PERSISTENT] == "true";
	}
	if (modifiers.find(PersistenceFields::RETRY_INTERVAL) != modifiers.end()) {
		persistence.retryInterval = std::strtoll(modifiers[PersistenceFields::RETRY_INTERVAL].c_str(), NULL, 0);
	}
	if (modifiers.find(PersistenceFields::RETRY_COUNT) != modifiers.end()) {
		persistence.retryCount = std::strtoll(modifiers[PersistenceFields::RETRY_COUNT].c_str(), NULL, 0);
	}
	if (modifiers.find(PersistenceFields::TOLERANCE_COUNT) != modifiers.end()) {
		persistence.toleranceCount = std::strtoll(modifiers[PersistenceFields::TOLERANCE_COUNT].c_str(), NULL, 0);
	}
	msg.persistence = persistence;

	if (modifiers.find(MessageFields::SUPPRESS_ACK) != modifiers.end()) {
		boost::to_lower(modifiers[MessageFields::SUPPRESS_ACK]);
		msg.suppressAck = modifiers[MessageFields::SUPPRESS_ACK] == "true";
	}
	if (modifiers.find(MessageFields::SUPPRESS_CONFIRM) != modifiers.end()) {
		boost::to_lower(modifiers[MessageFields::SUPPRESS_CONFIRM]);
		msg.suppressConfirm = modifiers[MessageFields::SUPPRESS_CONFIRM] == "true";
	}

	msg.sock = trigger.sock;

	message = msg;
}
