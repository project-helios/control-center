//
// Created by soorya on 10/12/17.
//

#include "Middleware/Constants.hpp"

const std::string MessageTypes::TRIGGER = "trigger";
const std::string MessageTypes::ACK = "ack";
const std::string MessageTypes::WARNING = "warn";
const std::string MessageTypes::CONFIRM = "confirm";

const std::string MessageFields::SUPPRESS_ACK = "suppressAck";
const std::string MessageFields::SUPPRESS_CONFIRM = "suppressConfirm";

const std::string PersistenceFields::IS_PERSISTENT = "isPersistent";
const std::string PersistenceFields::RETRY_INTERVAL = "retryInterval";
const std::string PersistenceFields::RETRY_COUNT = "retryCount";
const std::string PersistenceFields::TOLERANCE_COUNT = "toleranceCount";