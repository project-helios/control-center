//
// Created by soorya on 20/12/17.
//

#include "Middleware/TriggerStringTranslationTools/TriggerStringTranslationTools.hpp"


bool TriggerStringTranslationTools::getWorkerName(std::string &str, std::string &workerName) {
	boost::smatch what;
	boost::regex expr_workerName{"[a-zA-Z0-9_]+@"};
	std::string ans;
	if (boost::regex_search(str, what, expr_workerName)) {
		std::string s = what[0];
		boost::trim_right_if(s, [](char c) -> bool { return c == '@'; });
		ans = s;
	} else {
		return false;
	}
	workerName = ans;
	return true;
}

bool TriggerStringTranslationTools::getControllerName(std::string &str, std::string controllerName) {
	boost::smatch what;
	boost::regex expr_controllerName{"@[a-zA-Z0-9_]+:"};
	std::string ans;
	if (boost::regex_search(str, what, expr_controllerName)) {
		std::string s = what[0];
		boost::trim_left_if(s, [](char c) -> bool { return c == '@'; });
		boost::trim_right_if(s, [](char c) -> bool { return c == ':'; });
		boost::replace_first(str, s, "");
		ans = s;
	} else {
		return false;
	}
	controllerName = ans;
	return true;
}

void TriggerStringTranslationTools::getKwargsAndModifiers(std::string &str, std::map<std::string, std::string> kwargs,
														  std::map<std::string, std::string> modifiers) {
	boost::smatch what;

	kwargs.clear();
	modifiers.clear();
	boost::regex expr_kwargs{"(--)?([a-zA-Z0-9_]+)=<(.*?)>"};

	boost::sregex_token_iterator iter(str.begin(), str.end(), expr_kwargs, 0);
	boost::sregex_token_iterator end;

	for (; iter != end; ++iter) {
		std::string s = *iter;
		std::string kw, arg;
		boost::regex expr_kw{"(--)?([a-zA-Z0-9_]+)="};
		if (boost::regex_search(s, what, expr_kw)) {
			kw = what[0];
			boost::trim_right_if(kw, [](char c) -> bool { return c == '='; });
		}

		boost::regex expr_arg{"<(.*?)>"};
		if (boost::regex_search(s, what, expr_arg)) {
			arg = what[0];
			boost::trim_left_if(arg, [](char c) -> bool { return c == '<'; });
			boost::trim_right_if(arg, [](char c) -> bool { return c == '>'; });
		}
		if (kw.find("--") == 0) {
			boost::trim_left_if(kw, [](char c) -> bool { return c == '-'; });
			modifiers[kw] = arg;
//				cout << "Extracted modifier pair: " << kw << " -> " << arg << endl;
		} else {
			kwargs[kw] = arg;
//				cout << "Extracted keyword pair: " << kw << " -> " << arg << endl;
		}
	}
}

bool TriggerStringTranslationTools::getIdentity(std::string &str, Identity &identity) {
	boost::smatch what;
	boost::regex expr_controllerName{"\\[[a-zA-Z0-9_]+\\]"};
	std::string ans;
	if (boost::regex_search(str, what, expr_controllerName)) {
		std::string s = what[0];
		boost::trim_left_if(s, [](char c) -> bool { return c == '['; });
		boost::trim_right_if(s, [](char c) -> bool { return c == ']'; });
		boost::replace_first(str, s, "");
		ans = s;
	} else {
		return false;
	}
	identity.username = ans;
	return true;
}
