//
// Created by soorya on 20/12/17.
//

#include <iostream>
#include <thread>
#include <chrono>
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"
using namespace std;

int main() {
	while(true) {
		cout << "Hello world!" << endl;
		std::this_thread::sleep_for (std::chrono::seconds(1));
	}
	return 0;
}
#pragma clang diagnostic pop