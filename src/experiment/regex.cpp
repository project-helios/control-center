//
// Created by soorya on 10/12/17.
//

#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>
#include <iostream>

using namespace std;

int main() {
	string str = "fn_sq@helios:mode=<myMode>,kwarg1=<myKwarg1>,kwarg2=<myKwar@g2> --timeout=<3> --isPersistent=<true>";
	cout << "Original trigger: " << str << endl;
	boost::smatch what;

	cout << "Finding worker name: ";
	boost::regex expr_workerName{"[a-zA-Z0-9_]+@"};
	if (boost::regex_search(str, what, expr_workerName)) {
		for (int i = 0; i < what.size(); i++) {
			string s = what[i];
			boost::trim_right_if(s, [](char c) -> bool { return c == '@'; });
			boost::replace_first(str, s, "");
			std::cout << s << " ";
		}
	}
	cout << endl;

	cout << "Finding controller name: ";
	boost::regex expr_controllerName{"@[a-zA-Z0-9_]+:"};
	if (boost::regex_search(str, what, expr_controllerName)) {
		for (int i = 0; i < what.size(); i++) {
			string s = what[i];
			boost::trim_left_if(s, [](char c) -> bool { return c == '@'; });
			boost::trim_right_if(s, [](char c) -> bool { return c == ':'; });
			boost::replace_first(str, s, "");
			std::cout << s << " ";
		}
	}
	cout << endl;

	map<string, string> kwargs;
	map<string, string> modifiers;
	boost::regex expr_kwargs{"(--)?([a-zA-Z0-9_]+)=<(.*?)>"};

	boost::sregex_token_iterator iter(str.begin(), str.end(), expr_kwargs, 0);
	boost::sregex_token_iterator end;

	for (; iter != end; ++iter) {
		string s = *iter;
		string kw, arg;
		boost::regex expr_kw{"(--)?([a-zA-Z0-9_]+)="};
		if (boost::regex_search(s, what, expr_kw)) {
			kw = what[0];
			boost::trim_right_if(kw, [](char c) -> bool { return c == '='; });
		}

		boost::regex expr_arg{"<(.*?)>"};
		if (boost::regex_search(s, what, expr_arg)) {
			arg = what[0];
			boost::trim_left_if(arg, [](char c) -> bool { return c == '<'; });
			boost::trim_right_if(arg, [](char c) -> bool { return c == '>'; });
		}
		if (kw.find("--") == 0) {
			boost::trim_left_if(kw, [](char c) -> bool { return c == '-'; });
			modifiers[kw] = arg;
			cout << "Extracted modifier pair: " << kw << " -> " << arg << endl;
		} else {
			kwargs[kw] = arg;
			cout << "Extracted keyword pair: " << kw << " -> " << arg << endl;
		}
	}
	cout << endl;

//	cout << "Parsed trigger: " << str << endl;
	std::cout << "Search complete!" << std::endl;
}